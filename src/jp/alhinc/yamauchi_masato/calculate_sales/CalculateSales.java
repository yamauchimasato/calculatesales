package jp.alhinc.yamauchi_masato.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str){
				return new File(file,str).isFile() && str.matches("^\\d{8}.rcd$"); //ファイルかチェック後、名前の前半数字チェック8回,後半rcd
			}
		};

		Map<String,String> branchNames = new HashMap<>();
		Map<String,Long> branchSales = new HashMap<>();

		try {

			BufferedReader br = null;

			File branchLst = new File(args[0], "branch.lst");
			if (!branchLst.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			//支店定義ファイル読み込み
			try {
				br = new BufferedReader(new FileReader(branchLst));
				String line;
				while((line = br.readLine()) != null) {
					String[] array = line.split(",", -1);
					//支店定義ファイルのフォーマットチェック
					if(array.length != 2 || !array[0].matches("^\\d{3}$")) {
						//フォーマットが不正のとき
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					//フォーマットが正しいとき
					branchNames.put(array[0], array[1]);
					branchSales.put(array[0], 0L);
				}
			} finally {
				br.close();
			}

			//売り上げファイル読み込み(集計)
			BufferedReader br2 = null;
			try {
				//フィルターをかけてディレクトリからファイルを取得
				File[] rcdFiles = new File(args[0]).listFiles(filter);
				Arrays.sort(rcdFiles);
				List<Integer> list = new ArrayList<>();
				for(File rcdFile : rcdFiles) {
					list.add(Integer.parseInt(rcdFile.getName().substring(0, 8)));//リストに追加
				}
				//ファイル名の歯抜けチェック
				if(list.get(0) + list.size() -1 != list.get(list.size() - 1)) {
					//売り上げファイル名が歯抜けのとき
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				//歯抜けではないとき、ファイルからデータの読み込みを行う
				for(File rcdFile : rcdFiles) {
					br2 = new BufferedReader(new FileReader(rcdFile));
					String line1 = br2.readLine();
					String line2 = br2.readLine();
					String line3 = br2.readLine();
					String filename = rcdFile.getName();

					if(line3 != null) {
						System.out.println(filename + "のフォーマットが不正です");
						return;
					}

					//読み込んだ支店コードがリストに存在するかチェック
					if(!branchNames.containsKey(line1)) {
						//支店コードが存在しないとき
						System.out.println(filename + "の支店コードが不正です");
						return;
					}

					Long value = branchSales.get(line1) + Long.parseLong(line2);
					//合計金額の桁数チェック
					if(value.toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					branchSales.put(line1, value);
				}

			} finally {
				br2.close();
			}

			//集計結果出力
			BufferedWriter bw = null;
			try {
				File newfile = new File(args[0], "branch.out");//ファイル作成先パスとファイル名を指定
				bw = new BufferedWriter(new FileWriter(newfile));
				for (Entry<String, String> entry : branchNames.entrySet()) {
					bw.write(entry.getKey() + "," + entry.getValue() + "," + branchSales.get(entry.getKey()));
					bw.newLine();
				}

			} finally {
				bw.close();
			}

		}//全体try
		catch(Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}//main
} //CalculateSales

